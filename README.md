# telnet_container

Crea una imagen `telnet_container` a partir de una imagen ubuntu de manera que las siguientes salidas por consola coincidan:


```bash
➜  telnet_container git:(main) ✗ docker run --rm -it telnet_container www.ua.es 443
Trying 193.145.235.30...
Connected to vuala.ua.es.
Escape character is '^]'
```

```bash
➜  telnet_container git:(main) ✗ docker run --rm -it telnet_container  www.ua.es 80
Trying 193.145.235.30...
Connected to vuala.ua.es.
Escape character is '^]'
```

- ¿Serías capaz de añadir `docker run --rm -it telnet_container` como un alias de PowerShell con el nombre telnet-docker?